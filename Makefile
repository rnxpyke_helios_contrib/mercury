.POSIX:
.SUFFIXES:

all: scripts bootstrap.tar

include config.mk
include mk/$(ARCH).mk
include proto/Makefile
include scripts/Makefile

sbin/sysinit: protos
	mkdir -p sbin
	HAREPATH=. \
		$(HARE) build -t$(HAREARCH) \
		-X^ -T+$(HAREARCH) -T+init \
		-o sbin/sysinit cmd/sysinit/
bootstrap.tar: sbin/sysinit
.PHONY: sbin/sysinit

sbin/usrinit: protos
	mkdir -p sbin
	HAREPATH=. \
		$(HARE) build -t$(HAREARCH) \
		-X^ -T+$(HAREARCH) -T+threads \
		-o sbin/usrinit cmd/usrinit/
bootstrap.tar: sbin/usrinit
.PHONY: sbin/usrinit

cmd/ahci/manifest.o: cmd/ahci/manifest.ini
	objcopy -S -Ibinary -Oelf64-x86-64 \
		--rename-section .data=.manifest,contents \
		$< $@

sbin/drv/ahci: protos cmd/ahci/manifest.o
	mkdir -p sbin/drv
	# XXX: This is a bit messy
	LDLINKFLAGS=cmd/ahci/manifest.o \
	HAREPATH=. \
		$(HARE) build -t$(HAREARCH) \
		-X^ -T+$(HAREARCH) \
		-o sbin/drv/ahci cmd/ahci/
bootstrap.tar: sbin/drv/ahci
.PHONY: sbin/drv/ahci

cmd/serial/manifest.o: cmd/serial/manifest.ini
	objcopy -S -Ibinary -Oelf64-x86-64 \
		--rename-section .data=.manifest,contents \
		$< $@

sbin/drv/serial: protos cmd/serial/manifest.o
	mkdir -p sbin/drv
	# XXX: This is a bit messy
	LDLINKFLAGS=cmd/serial/manifest.o \
	HAREPATH=. \
		$(HARE) build -t$(HAREARCH) \
		-X^ -T+$(HAREARCH) \
		-o sbin/drv/serial cmd/serial/
bootstrap.tar: sbin/drv/serial
.PHONY: sbin/drv/serial

cmd/pcibus/manifest.o: cmd/pcibus/manifest.ini
	objcopy -S -Ibinary -Oelf64-x86-64 \
		--rename-section .data=.manifest,contents \
		$< $@

sbin/drv/pcibus: protos cmd/pcibus/manifest.o
	mkdir -p sbin/drv
	# XXX: This is a bit messy
	LDLINKFLAGS=cmd/pcibus/manifest.o \
	HAREPATH=. \
		$(HARE) build -t$(HAREARCH) \
		-X^ -T+$(HAREARCH) \
		-o sbin/drv/pcibus cmd/pcibus/
bootstrap.tar: sbin/drv/pcibus
.PHONY: sbin/drv/pcibus

cmd/ps2kb/manifest.o: cmd/ps2kb/manifest.ini
	objcopy -S -Ibinary -Oelf64-x86-64 \
		--rename-section .data=.manifest,contents \
		$< $@

sbin/drv/ps2kb: protos cmd/ps2kb/manifest.o
	mkdir -p sbin/drv
	# XXX: This is a bit messy
	LDLINKFLAGS=cmd/ps2kb/manifest.o \
	HAREPATH=. \
		$(HARE) build -t$(HAREARCH) \
		-X^ -T+$(HAREARCH) \
		-o sbin/drv/ps2kb cmd/ps2kb/
bootstrap.tar: sbin/drv/ps2kb
.PHONY: sbin/drv/ps2kb

cmd/vgacons/manifest.o: cmd/vgacons/manifest.ini
	objcopy -S -Ibinary -Oelf64-x86-64 \
		--rename-section .data=.manifest,contents \
		$< $@

sbin/drv/vgacons: protos cmd/vgacons/manifest.o
	mkdir -p sbin/drv
	# XXX: This is a bit messy
	LDLINKFLAGS=cmd/vgacons/manifest.o \
	HAREPATH=. \
		$(HARE) build -t$(HAREARCH) \
		-X^ -T+$(HAREARCH) \
		-o sbin/drv/vgacons cmd/vgacons/
bootstrap.tar: sbin/drv/vgacons
.PHONY: sbin/drv/vgacons

bootstrap.tar:
	tar -cvf bootstrap.tar etc sbin README.md

$(HELIOS)/helios:
	cd $(HELIOS) && make helios

.PHONY: $(HELIOS)/helios

install-libs:
	mkdir -p "$(DESTDIR)$(SRCDIR)/hare/mercury"
	./scripts/install-mods "$(DESTDIR)$(SRCDIR)/hare/mercury"

install-kernel:
	mkdir -p "$(DESTDIR)$(LIBEXECDIR)"/helios
	install -Dm755 "$(HELIOS)/helios" "$(DESTDIR)$(LIBEXECDIR)"/helios/helios-$(ARCH)

.PHONY: install-libs install-kernel install-arch

clean:
	rm -rf sbin
	rm -f cmd/serial/manifest.o

install: install-libs
install: install-kernel
install: install-arch

.PHONY: clean install
