PHDRS {
	headers PT_PHDR PHDRS;
	text PT_LOAD FILEHDR PHDRS;
	data PT_LOAD;
	tls PT_NULL;
	manifest PT_NULL;
}
ENTRY(_start);
SECTIONS {
	. = 0x800000;
	.text : {
		KEEP (*(.text))
		*(.text.*)
	} :text

	. = ALIGN(4096);
	.data : {
		KEEP (*(.data))
		*(.data.*)
	} :data

	.bss : {
		KEEP (*(.bss))
		*(.bss.*)
	} :data

	.init_array : {
		PROVIDE_HIDDEN (__init_array_start = .);
		KEEP (*(.init_array))
		PROVIDE_HIDDEN (__init_array_end = .);
	} :data

	.fini_array : {
		PROVIDE_HIDDEN (__fini_array_start = .);
		KEEP (*(.fini_array))
		PROVIDE_HIDDEN (__fini_array_end = .);
	} :data

	.test_array : {
		PROVIDE_HIDDEN (__test_array_start = .);
		KEEP (*(.test_array))
		PROVIDE_HIDDEN (__test_array_end = .);
	} :data

	. = 0;
	.tdata : {
		KEEP (*(.tdata))
		*(.tdata)
	} :tls

	.tbss : {
		KEEP (*(.tbss))
		*(.tbss)
	} :tls

	.manifest : {
		KEEP (*(.manifest))
		*(.manifest)
	} :manifest
}
