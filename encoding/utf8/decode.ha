// License: MPL-2.0
// (c) 2021 Bor Grošelj Simić <bor.groseljsimic@telemach.net>
// (c) 2021 Drew DeVault <sir@cmpwn.com>
// (c) 2021 Ember Sawady <ecs@d2evs.net>
use types;

fn toutf8(in: str) []u8 = *(&in: *[]u8);

export type decoder = struct {
	offs: size,
	src: []u8,
};

// Initializes a new UTF-8 decoder.
export fn decode(src: (str | []u8)) decoder = match (src) {
case let s: str =>
	yield decoder { src = toutf8(s), ...  };
case let b: []u8 =>
	yield decoder { src = b, ...  };
};

// Returned when more data is needed, i.e. when an incomplete UTF-8 sequence is
// encountered.
export type more = void;

// Returned when an invalid UTF-8 sequence was found.
export type invalid = !void;

// Returns the next rune from a decoder. void is returned when there are no
// remaining codepoints.
export fn next(d: *decoder) (rune | void | more | invalid) = {
	assert(d.offs <= len(d.src));
	if (d.offs == len(d.src)) {
		return;
	};

	// XXX: It would be faster if we decoded and measured at the same time.
	const n = match (utf8sz(d.src[d.offs])) {
	case let z: size =>
		yield z;
	case void =>
		return invalid;
	};
	if (d.offs + n > len(d.src)) {
		return more;
	};
	let bytes = d.src[d.offs..d.offs+n];
	d.offs += n;

	let r = 0u32;
	if (bytes[0] < 128) {
		// ASCII
		return bytes[0]: u32: rune;
	};

	const mask = masks[n - 1];
	r = bytes[0] & mask;
	for (let i = 1z; i < len(bytes); i += 1) {
		r <<= 6;
		r |= bytes[i] & 0x3F;
	};
	return r: rune;
};

// Returns the previous rune from a decoder. void is returned when there are no
// previous codepoints.
export fn prev(d: *decoder) (rune | void | more | invalid) = {
	if (d.offs == 0) {
		return;
	};

	let n = 0z;
	let r = 0u32;

	for (let i = 0z; i < d.offs; i += 1) {
		if ((d.src[d.offs - i - 1] & 0xC0) == 0x80) {
			let tmp: u32 = d.src[d.offs - i - 1] & 0x3F;
			r |= tmp << (i * 6): u32;
		} else {
			n = i + 1;
			let tmp: u32 = d.src[d.offs - i - 1] & masks[i];
			r |=  tmp << (i * 6): u32;
			break;
		};
	};
	if (n == 0) {
		return more;
	};
	d.offs -= n;
	match (utf8sz(d.src[d.offs])) {
	case let z: size =>
		return if (n == z) r: rune else invalid;
	case void =>
		return invalid;
	};
};

// Returns true if a given string or byte slice contains only valid UTF-8
// sequences. Note that Hare strings (str) are always valid UTF-8 - if this
// returns false for a str type, something funny is going on.
export fn valid(src: (str | []u8)) bool = {
	let decoder = decode(src);
	for (true) {
		match (next(&decoder)) {
		case void =>
			return true;
		case invalid =>
			return false;
		case more =>
			return false;
		case rune => void;
		};
	};
	abort();
};

// Returns the expected length of a UTF-8 codepoint in bytes given its first
// byte, or void if the given byte doesn't begin a valid UTF-8 sequence.
export fn utf8sz(c: u8) (size | void) = {
	for (let i = 0z; i < len(sizes); i += 1) {
		if (c & sizes[i].mask == sizes[i].result) {
			return sizes[i].octets;
		};
	};
};
