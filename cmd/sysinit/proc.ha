use errors;
use format::elf;
use helios;
use helios::{ctype, map_flags};
use io;
use rt;
use serv::sys;
use strings;

export type process = struct {
	sys::process,
	sysinit: *sysinit_state,
	pid: uint,
	args: []str,
	task: helios::cap,
	vspace: helios::cap,
	cspace: helios::cap,
	// TODO: merge image and highpg into pages?
	image: []helios::cap,
	highpg: []helios::cap,
	loader: nullable *loader,
};

// Creates a new process, allocating a task, vspace, and cspace with the given
// radix, and allocates and maps a stack and IPC buffer.
export fn newprocess(
	sysinit: *sysinit_state,
	radix: uint,
	args: []str,
) (process | errors::error) = {
	let ld = newloader();
	let proc = process {
		_iface = &proc_impl,
		_endpoint = helios::newendpoint()?,
		sysinit = sysinit,
		// XXX: Might be better to source this from the aux vector
		args = strings::dupall(args),

		task = helios::newtask()?,
		vspace = helios::newvspace()?,
		cspace = helios::newcspace(radix)?,
		loader = ld,

		...
	};
	helios::asid_pool_assign(rt::INIT_CAP_ASID_POOL, proc.vspace)?;
	ld.iv = initaux(&proc, args)?;
	return proc;
};

const proc_impl = sys::process_iface {
	load = &proc_load,
	install = &proc_install,
	start = &proc_start,
	suspend = &proc_suspend,
};

fn proc_load(
	obj: *sys::process,
	image: helios::cap,
) void = {
	let proc = obj: *process;
	let ld = proc.loader as *loader; // TODO: Error handling
	defer helios::destroy(image)!;
	const image = image: io::file;
	load(ld, proc, image)!;
};

fn proc_install(
	obj: *sys::process,
	caps: []helios::cap,
) void = {
	let proc = obj: *process;
	assert(proc.loader != null); // TODO
};

fn proc_start(obj: *sys::process) void = {
	let proc = obj: *process;
	const ld = match (proc.loader) {
	case let ld: *loader =>
		yield ld;
	case null =>
		helios::task_resume(proc.task)!;
		return;
	};

	// TODO: Error handling
	loader_finalize(ld, proc)!;
	free(proc.loader);
	proc.loader = null;

	helios::task_resume(proc.task)!;
};

fn proc_suspend(obj: *sys::process) void = {
	abort(); // TODO
};

// Adds a capability to a process. The "at" should be NULL if no auxv entry
// should be created.
fn proc_addcap(
	proc: *process,
	cap: helios::cap,
	at: elf::at,
) (helios::cap | errors::error) = {
	const cap = helios::cspace_copyto(
		proc.cspace, helios::CADDR_UNDEF,
		rt::cspace, cap)?;

	if (at != elf::at::NULL) {
		auxv_add(proc, &elf::auxv64 {
			a_type = at,
			a_val = cap: u64,
		});
	};

	return cap;
};
