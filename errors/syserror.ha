use rt;

// The given capability address exceeds the bounds of its cspace.
export type invalid_caddr = !void;

// The desired cslot is already occupied by another capability.
export type invalid_cslot = !void;

// A provided capability is not the expected type for the requested operation.
export type invalid_ctype = !void;

// Wraps an [[rt::syserror]] to produce an [[errors::opaque]]. This is a
// non-portable interface which is mainly provided to support internal stdlib
// requirements.
export fn syserror(err: rt::syserror) error = {
	// TODO: Add errors:: types for each of these
	match (err) {
	case rt::nomem =>
		return nomem;
	case rt::unsupported =>
		return unsupported;
	case rt::would_block =>
		return again;
	case rt::invalid_caddr =>
		return invalid_caddr;
	case rt::invalid_cslot =>
		return invalid_cslot;
	case rt::invalid_ctype =>
		return invalid_ctype;
	case =>
		yield;
	};

	static assert(size(rt::syserror) <= size(opaque_data));
	let opaque = opaque { strerror = &rt_strerror, ... };
	let ptr = &opaque.data: *rt::syserror;
	*ptr = err;
	return opaque;
};

// Wraps a syscall invocation and returns either the result or an [[error]].
export fn syswrap(ret: (u64 | rt::syserror)) (u64 | error) = {
	match (ret) {
	case let v: u64 =>
		return v;
	case let err: rt::syserror =>
		return syserror(err);
	};
};

fn rt_strerror(err: *opaque_data) const str = {
	let err = err: *rt::syserror;
	return rt::strerror(*err);
};
