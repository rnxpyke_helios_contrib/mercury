.global rt.fetch_add
rt.fetch_add:
	lock xadd %rsi, (%rdi)
	mov %rsi, %rax
	ret

.global rt.barrier
rt.barrier:
	mfence
	ret
