use bus::pci;
use helios;

def PORT_CONF_ADDR: u16 = 0xCF8;
def PORT_CONF_DATA: u16 = 0xCFC;
def IOPORT_CONF: helios::cap = 0;

fn pci_addr(device: u32, offs: u8) u32 = {
	return 0x80000000u32
		| pci::device_bus(device): u32 << 16
		| pci::device_slot(device): u32 << 11
		| pci::device_func(device): u32 << 8
		| (offs & 0xFC);
};

fn writeaddr(device: u32, offs: u8) void = {
	helios::ioport_out32(IOPORT_CONF, PORT_CONF_ADDR, pci_addr(device, offs))!;
};

// Reads a 32-bit register from PCI configuration space.
fn readl(device: u32, offs: u8) u32 = {
	writeaddr(device, offs);
	return helios::ioport_in32(IOPORT_CONF, PORT_CONF_DATA)!;
};

// Reads a 16-bit register from PCI configuration space.
fn readw(device: u32, offs: u8) u16 = {
	writeaddr(device, offs);
	return helios::ioport_in16(IOPORT_CONF, PORT_CONF_DATA + (offs & 2))!;
};

// Reads an 8-bit register from PCI configuration space.
fn readb(device: u32, offs: u8) u8 = {
	writeaddr(device, offs);
	return helios::ioport_in8(IOPORT_CONF, PORT_CONF_DATA + (offs & 3))!;
};

// Writes a value to a PCI config register.
fn writel(device: u32, offs: u8, val: u32) void = {
	assert(offs % 4 == 0); // TODO: Unaligned writes
	writeaddr(device, offs);
	helios::ioport_out32(IOPORT_CONF, PORT_CONF_DATA, val)!;
};

// Returns the device class/subclass for a given device.
fn read_type(dev: u32) u16 = {
	return ((readb(dev, pci::PCI_CLASS): u16 << 8)
		| readb(dev, pci::PCI_SUBCLASS): u16);
};
