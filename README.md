# Mercury

Mercury is a userspace for the [Helios][0] micro-kernel which provides essential
low-level userspace services and a framework for hardware drivers.

[0]: https://git.sr.ht/~sircmpwn/helios

## Building Mercury

Mercury has the same dependencies as Helios, plus [ipcgen][1], which must be
installed to your $PATH when building.

[1]: https://git.sr.ht/~sircmpwn/ipcgen

Copy `config.def.mk` to `config.mk` and edit it to taste. You need to provide
the path to a Helios source tree in the HELIOS variable; it will be
automatically compiled when building Mercury.

Run `make` to build boot.iso, which can be written to a USB stick and booted on
hardware, or `make run` to boot it in qemu. `make nographic` will boot with a
serial console attached to stdin/stdout rather than opening a virtual display;
this is generally the more useful approach for testing.
