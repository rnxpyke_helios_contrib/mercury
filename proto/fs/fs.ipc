namespace sys;
use errors;
use io;

# Flags for [[fs::open]].
enum flags {
	RDONLY		= 0,
	WRONLY		= 1,
	RDWR		= 2,
	CREATE		= 0o100,
	TRUNC		= 0o1000,
	APPEND		= 0o2000,
	DIRECTORY	= 0o200000,
	NOFOLLOW	= 0o400000,
};

# A filesystem object.
#
# Paths are specified by a pair of arguments and one or more page capabilities.
# The first argument is a "path" pointer which represents the offset from the
# page at which a UTF-8 string of up to "n" bytes can be found, forming the
# desired path.
#
# Filesystems which support path names which are not valid UTF-8 will have
# their paths coerced into a UTF-8 compatible form in a consistent but
# implementation-defined manner.
interface fs {
	# Opens a file at the given path with the given flags and an optional
	# file mode (create only).
	#
	# Opening directory nodes with RDONLY | DIRECTORY will return a file
	# object from which directory entries may be read. Such files are not
	# seekable.
	#
	# Opening symbolic link nodes with RDONLY | NOFOLLOW will return a file
	# object from which the target path may be read as a UTF-8 string. Such
	# files are not seekable.
	call open{pages: page...; out: io::file}(
		path: uintptr,
		n: size,
		flags: flags,
		mode: uint,
	) void | errors::noentry | errors::wrongtype | errors::noaccess;

	# Clones this filesystem, producing a new filesystem object which may
	# optionally be rooted at a different path (or "/" for the same view of
	# the filesystem).
	call clone{; fs}(path: uintptr, n: size) void | errors::unsupported;

	# TODO: rename, delete, mkdir, link, etc
};
