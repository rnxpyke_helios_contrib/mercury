HAREARCH = aarch64
QBEARCH = arm64
QEMUARCH = aarch64
QEMUFLAGS = -cpu cortex-a76
AAVMF = /usr/share/AAVMF

all: bootfat.img

# XXX: This code probably has too much knowledge of Helios internals
BOOT=$(HELIOS)/boot/+aarch64

$(BOOT)/bootaa64.efi:
	cd ../helios && make boot/+aarch64/bootaa64.efi

.PHONY: $(BOOT)/bootaa64.efi

$(BOOT)/qemu.dtb:
	cd ../helios && make boot/+aarch64/qemu.dtb

.PHONY: $(BOOT)/qemu.dtb

bootfat.img: $(BOOT)/bootaa64.efi $(BOOT)/qemu.dtb $(HELIOS)/helios init bootstrap.tar
	dd if=/dev/zero of=$@ bs=512 count=93750 status=none
	parted $@ -s -a minimal mklabel gpt
	parted $@ -s -a minimal mkpart EFI FAT16 2048s 93716s
	parted $@ -s -a minimal toggle 1 boot
	dd if=/dev/zero of=$(BOOT)/part.img bs=512 count=91668 status=none # 93750 - 2048
	mformat -i $(BOOT)/part.img -h 32 -t 32 -n 64 -c 1
	mmd -i $(BOOT)/part.img ::EFI
	mmd -i $(BOOT)/part.img ::EFI/boot
	mmd -i $(BOOT)/part.img ::modules
	mcopy -i $(BOOT)/part.img $(BOOT)/bootaa64.efi ::EFI/boot/bootaa64.efi
	mcopy -i $(BOOT)/part.img $(BOOT)/qemu.dtb ::helios.dtb
	mcopy -i $(BOOT)/part.img $(HELIOS)/helios ::helios
	mcopy -i $(BOOT)/part.img init ::modules/00-init
	mcopy -i $(BOOT)/part.img bootstrap.tar ::modules/01-bootstrap.tar
	dd if=$(BOOT)/part.img of=$@ bs=512 count=91668 seek=2048 conv=notrunc status=none

nographic: bootfat.img
	qemu-system-$(QEMUARCH) -m 1G -no-reboot -no-shutdown -M virt \
		$(QEMUFLAGS) \
		-drive if=pflash,file=$(AAVMF)/AAVMF_CODE.fd,format=raw,read-only=on \
		-drive if=none,file=bootfat.img,id=hd0,format=raw \
		-device virtio-blk-device,drive=hd0 \
		-display none \
		-serial stdio
