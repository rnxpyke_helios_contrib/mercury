use helios;
use helios::{pollflags};
use serv::dev;
use sys;

def IOPORT: helios::cap = 0;
def IRQ_NOTE: helios::cap = 1;
def IRQ1: helios::cap = 2;

type ps2kb = struct {
	dev::kb,
	buf: [8]u64,
	buffer: []u64,
	reply: helios::cap,
};

export fn main() void = {
	let ep: helios::cap = 0;
	const registry = helios::service(sys::DEVREGISTRY_ID);
	sys::devregistry_new(registry, dev::KB_ID, &ep);
	helios::destroy(registry)!;

	helios::irq_ack(IRQ1)!;

	const kb = ps2kb {
		_iface = &kb_impl,
		_endpoint = ep,
		...
	};
	kb.buffer = kb.buf[..0];
	const poll = [
		helios::pollcap {
			cap = IRQ_NOTE,
			events = pollflags::RECV,
			...
		},
		helios::pollcap {
			cap = ep,
			events = pollflags::RECV,
			...
		},
	];
	for (true) {
		helios::poll(poll)!;

		if (poll[0].revents & pollflags::RECV != 0) {
			poll_irq(&kb);
		};
		if (poll[1].revents & pollflags::RECV != 0) {
			poll_ep(&kb);
		};
	};
};

fn poll_irq(kb: *ps2kb) void = {
	helios::wait(IRQ_NOTE)!;
	let scancode = helios::ioport_in8(IOPORT, 0x60)!;
	helios::irq_ack(IRQ1)!;

	const mask = 1u8 << 7;
	const state = if (scancode & mask == 0) {
		yield dev::key_state::PRESSED;
	} else {
		yield dev::key_state::RELEASED;
	};
	scancode = (scancode & ~mask) + 8;

	if (len(kb.buffer) == len(kb.buf)) {
		return; // drop key
	};
	static append(kb.buffer, scancode | state: u64 << 32);

	if (kb.reply != 0) {
		const scancode = kb.buffer[0];
		static delete(kb.buffer[0]);
		helios::send(kb.reply, 0, scancode)!;
		kb.reply = 0;
	};
};

fn poll_ep(kb: *ps2kb) void = {
	dev::kb_dispatch(kb);
};

const kb_impl = dev::kb_iface {
	poll = &kb_poll,
	set_led = &kb_set_led,
};

fn kb_poll(obj: *dev::kb) u64 = {
	let kb = obj: *ps2kb;
	if (len(kb.buffer) != 0) {
		const scancode = kb.buffer[0];
		static delete(kb.buffer[0]);
		return scancode;
	};
	kb.reply = helios::store_reply(helios::CADDR_UNDEF)!;
	return 0;
};

fn kb_set_led(obj: *dev::kb, mask: dev::kb_led) void = {
	abort(); // TODO
};
