// License: MPL-2.0
// (c) 2021 Drew DeVault <sir@cmpwn.com>
// (c) 2021-2022 Ember Sawady <ecs@d2evs.net>
use types;

// Converts any [[types::signed]] to a string in a given base. The return value is
// statically allocated and will be overwritten on subsequent calls; see
// [[strings::dup]] to duplicate the result.
export fn signedtosb(n: types::signed, b: base) const str = {
	match (n) {
	case let i: int =>
		return itosb(i, b);
	case let i: i8 =>
		return i8tosb(i, b);
	case let i: i16 =>
		return i16tosb(i, b);
	case let i: i32 =>
		return i32tosb(i, b);
	case let i: i64 =>
		return i64tosb(i, b);
	};
};

// Converts any [[types::signed]] to a string in base 10. The return value is
// statically allocated and will be overwritten on subsequent calls; see
// [[strings::dup]] to duplicate the result.
export fn signedtos(n: types::signed) const str = signedtosb(n, base::DEC);

// Converts any [[types::unsigned]] to a string in a given base. The return value
// is statically allocated and will be overwritten on subsequent calls; see
// [[strings::dup]] to duplicate the result.
export fn unsignedtosb(n: types::unsigned, b: base) const str = {
	match (n) {
	case let u: size =>
		return ztosb(u, b);
	case let u: uint =>
		return utosb(u, b);
	case let u: u8 =>
		return u8tosb(u, b);
	case let u: u16 =>
		return u16tosb(u, b);
	case let u: u32 =>
		return u32tosb(u, b);
	case let u: u64 =>
		return u64tosb(u, b);
	};
};

// Converts any [[types::unsigned]] to a string in base 10. The return value is
// statically allocated and will be overwritten on subsequent calls; see
// [[strings::dup]] to duplicate the result.
export fn unsignedtos(n: types::unsigned) const str = unsignedtosb(n, base::DEC);

// Converts any [[types::integer]] to a string in a given base, which must be 2,
// 8, 10, or 16. The return value is statically allocated and will be
// overwritten on subsequent calls; see [[strings::dup]] to duplicate the result.
export fn integertosb(n: types::integer, b: base) const str = {
	match (n) {
	case let s: types::signed =>
		return signedtosb(s, b);
	case let u: types::unsigned =>
		return unsignedtosb(u, b);
	};
};

// Converts any [[types::integer]] to a string in base 10. The return value is
// statically allocated and will be overwritten on subsequent calls; see
// [[strings::dup]] to duplicate the result.
export fn integertos(n: types::integer) const str = integertosb(n, base::DEC);
