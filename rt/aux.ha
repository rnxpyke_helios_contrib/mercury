use format::elf;

export type iface_iterator = int;

// Enumerates the interfaces provided via the auxillary vector.
export fn iface_iter() iface_iterator = {
	return 0;
};

// Returns the next (interface ID, capability) from the list of startup
// capabilities. Returns (0, 0) after all capabilities have been enumerated.
export fn iface_next(iter: *iface_iterator) (u64, cap) = {
	for (auxv[*iter].a_type != elf::at::NULL; *iter += 1) {
		if (auxv[*iter].a_type != elf::at::CAP_IFACE) {
			continue;
		};
		const iface = auxv[*iter].a_val;
		*iter += 1;
		return (iface, auxv[*iter].a_val: cap);
	};
	return (0, 0);
};
