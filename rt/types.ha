// A capability address.
export type cap = uint;

// Capability rights.
export type rights = enum u8 {
	NONE = 0,
	SEND = 1 << 0,
	RECV = 1 << 1,
	ALL = SEND | RECV,
};

// Flags for the poll syscall.
export type pollflags = enum uint {
	NONE = 0,
	SEND = 1 << 0,
	RECV = 1 << 1,
};

// Capability address of the init task's cspace.
export def INIT_CAP_CSPACE: uint = 1;

// Capability address of the init task's vspace.
export def INIT_CAP_VSPACE: uint = 2;

// Capability address of the init task's ASID control.
export def INIT_CAP_ASID_CONTROL: uint = 3;

// Capability address of the init task's ASID pool.
export def INIT_CAP_ASID_POOL: uint = 4;

// Capability address of the init task itself.
export def INIT_CAP_TASK: uint = 5;

// Capability address of the init task's IPC buffer page.
def INIT_CAP_IPC_BUFFER: uint = 6;

// Address of first dynamically allocated capability.
export def INIT_CAP_RANGES_START: uint = 16;

// The bootinfo structure.
export type bootinfo = struct {
	argv: str,

	// Capability ranges
	memory: cap_range,
	devmem: cap_range,
	userimage: cap_range,
	stack: cap_range,
	bootinfo: cap_range,
	unused: cap_range,

	// Other details
	arch: *arch_bootinfo,
	ipcbuf: uintptr,
	modules: []module_desc,
	memory_info: []memory_desc,
	devmem_info: []memory_desc,
	tls_base: uintptr,
	tls_size: size,
};

// A continuous range of capabilities allocated for a single purpose.
export type cap_range = struct {
	// Inclusive
	start: uint,
	// Exclusive
	end: uint,
};

// A description of a region of memory.
export type memory_desc = struct {
	phys: uintptr,
	pages: uint,
};

// A module provided by the boot loader.
export type module_desc = struct {
	pages: cap_range,
	length: size,
};

export def MAX_CAP: size = 64;
export def MAX_PARAM: size = 64;

// The IPC buffer.
export type ipc_buffer = struct {
	// Message tag
	tag: u64,
	// Badge of the invoked capability, or zero
	badge: uint,
	// Registers
	params: [64]u64,
	// Capability addresses
	caps: [64]uint,
	// Flags for capabilities transferred
	cflags: [64]cflags,
};

// Flags that apply to capabilities in an IPC buffer.
export type cflags = enum uint {
	// Set by userspace on receive to indicate that a capability should be
	// unwrapped, not transferred, on receipt. Set by the kernel to confirm
	// that this occurred, or unset if the capability could not be unwrapped
	// (it's not an endpoint, or it is unbadged).
	UNWRAP = 1 << 0,
};
