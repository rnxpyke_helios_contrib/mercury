# Set this to the desired build architecture. Valid options include:
#
# - aarch64
# - x86_64
ARCH = x86_64

# Path to Helios checkout
HELIOS=../helios

# Install configuration
PREFIX=/usr/local
BINDIR=$(PREFIX)/bin
SRCDIR=$(PREFIX)/src
SHAREDIR=$(PREFIX)/share
LIBEXECDIR=$(PREFIX)/libexec

# Toolchain
HARE = hare
HAREC = harec
QBE = qbe
AS = as
LD = ld
IPCGEN = ipcgen

# syslinux location
SYSLINUX=/usr/share/syslinux
