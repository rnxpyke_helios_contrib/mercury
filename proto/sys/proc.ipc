namespace sys;
use io;

# Flags for process::mmap operations
# TODO: Add arch-specific flags?
enum map_flags {
	W = 1 << 0,
	X = 1 << 1,
};

# A process object.
interface process {
	# Loads an ELF image into a process. The process must be suspended
	# during the operation. The stack pointer will be reset and the program
	# counter register will be set to the ELF file's entry point.
	call load{image: io::file}() void;

	# Installs a capability or capabilities into a process's cspace at the
	# given capability addresses. Capabilities will be installed
	# sequentially in unoccupied slots starting from slot 0.
	#
	# May not be used after [[start]] is called.
	call install{caps...}() void;

	# Starts executing a process. Must have loaded an image first.
	call start() void;

	# Suspends a process, so it no longer is scheduled until [[start]] is
	# called again.
	call suspend() void;
};

# Process manager interface.
interface procmgr {
	# Creates a new process, reading the argument vector from the provided
	# buffer with the following format:
	#
	#	struct {
	#		nargs: size,
	#		args: [*]struct {
	#			length: size,
	#			data: [*]u8,	// UTF-8
	#		},
	#	};
	call new{pages: page...; proc: process}(buf: uintptr) void;

	# Populates the given buffer with a list of processes in the following
	# format:
	#
	# 	[*]struct {
	# 		pid: uint,
	# 		nargs: size,
	# 		args: [*]struct {
	# 			length: size,
	# 			data: [*]u8,
	# 			// Padded to alignment of 8
	# 		},
	# 	};
	#
	# Returns the number of processes written. If no pages are provided,
	# returns the total number of processes instead.
	call query{pages: page...}(addr: uintptr, length: size) size;
};
