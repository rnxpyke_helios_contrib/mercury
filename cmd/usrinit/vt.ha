use dev;
use dev::{key_state};
use fmt;
use helios;
use io;
use strio;

type videoterm = struct {
	term,
	kb: io::file,
	cons: io::file,
	shift: bool,
};

fn newvt(kb: io::file, cons: io::file) videoterm = {
	dev::console_set_cursor_mode(cons, dev::cursor_mode::NORMAL);
	return videoterm {
		get_event = &vt_get_event,
		get_output = &vt_get_output,
		kb = kb,
		cons = cons,
		shift = false,
	};
};

fn vt_get_output(term: *term) io::file = {
	const term = term: *videoterm;
	return term.cons;
};

// Returns the next event from the console.
fn vt_get_event(term: *term) (tkey, rune) = {
	let term = term: *videoterm;

	for (true) {
		const ev = dev::kb_poll(term.kb);
		const scancode = ev & ~0u32;
		const state = (ev >> 32): dev::key_state;

		switch (scancode) {
		case K_LFSH, K_RTSH =>
			term.shift = state == dev::key_state::PRESSED;
		case =>
			yield;
		};

		const keycode = scancode: size - 8;
		if (state != key_state::PRESSED || keycode >= len(keysyms)) {
			continue;
		};

		let layer = if (term.shift) 1 else 0;
		const sym = keysyms[keycode][layer];
		if (sym == '\b') {
			return (tkey::BACKSPACE, sym);
		};
		if (sym != '\0') {
			return (tkey::RUNE, sym);
		};
	};

	abort(); // Unreachable
};
