// License: MPL-2.0
// (c) 2021 Drew DeVault <sir@cmpwn.com>
// (c) 2021 Ember Sawady <ecs@d2evs.net>

// Converts an [[error]] into a human-friendly string representation.
//
// Note that this strerror implementation lacks any context-specific information
// about the error types supported. For example, [[exists]] is stringified as "An
// attempt was made to create a resource which already exists", but if source of
// the error is, say, creating a file, it would likely be more appropriate to
// use the term "file" rather than "resource". For this reason, it is preferred
// that modules which return an error type from this module provide their own
// strerror function which provides more context-appropriate error messages for
// each of those types.
export fn strerror(err: error) const str = {
	match (err) {
	case busy =>
		return "The requested resource is not available";
	case exists =>
		return "An attempt was made to create a resource which already exists";
	case invalid =>
		return "A function was called with an invalid combination of arguments";
	case noaccess =>
		return "The user does not have permission to use this resource";
	case noentry =>
		return "An entry was requested which does not exist";
	case wrongtype =>
		return "Wrong file type";
	case overflow =>
		return "The requested operation caused a numeric overflow condition";
	case unsupported =>
		return "The requested operation is not supported";
	case timeout =>
		return "The requested operation timed out";
	case cancelled =>
		return "The requested operation was cancelled";
	case refused =>
		return "A connection attempt was refused";
	case nomem =>
		return "Unable to allocate sufficient memory for the requested operation";
	case interrupted =>
		return "Operation interrupted";
	case again =>
		return "Try again";
	case invalid_caddr =>
		return "Invalid capability address";
	case invalid_cslot =>
		return "Invalid capability slot";
	case invalid_ctype =>
		return "Invalid capability type";
	case let op: opaque =>
		return op.strerror(&op.data);
	};
};
