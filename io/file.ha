// License: MPL-2.0
// (c) 2021 Bor Grošelj Simić <bor.groseljsimic@telemach.net>
// (c) 2021-2022 Drew DeVault <sir@cmpwn.com>
// (c) 2021 Ember Sawady <ecs@d2evs.net>
use errors;
use helios;
use helios::{map_flags};
use rt;
use strings;

// This is an opaque type which encloses an OS-level file handle resource. It
// can be used as a [[handle]] in most situations, but there are some APIs which
// require a [[file]] with some OS-level handle backing it - this type is used
// for such APIs.
//
// In Mercury, [[file]] is capability address.
export type file = helios::cap;

fn fd_read(fd: file, buf: []u8) (size | EOF | error) = {
	let n = len(buf);
	if (n > rt::PAGESIZE) {
		// TODO: read buffers >1 page
		n = rt::PAGESIZE;
	};

	// TODO: buffer pool?
	const (page, ptr, _) = mkbuffer(n);
	defer helios::destroy(page)!;

	const z = file_read(fd, [page], ptr, n);
	assert(z <= n);
	if (z == 0) {
		return EOF;
	};

	const data = ptr: *[*]u8;
	buf[..z] = data[..z];
	return z;
};

fn fd_write(fd: file, buf: const []u8) (size | error) = {
	let n = len(buf);
	if (n > rt::PAGESIZE) {
		// TODO: read buffers >1 page
		n = rt::PAGESIZE;
	};

	// TODO: buffer pool?
	const (page, ptr, _) = mkbuffer(n);
	defer helios::destroy(page)!;
	let data = ptr: *[*]u8;
	data[..n] = buf[..n];

	return file_write(fd, [page], ptr, n);
};

fn fd_close(fd: file) (void | error) = {
	abort(); // TODO
};

fn fd_seek(
	fd: file,
	offs: off,
	whence: whence,
) (off | error) = {
	return file_seek(fd, offs, whence): off;
};

fn mkbuffer(n: size) (helios::cap, uintptr, size) = {
	assert(n <= rt::PAGESIZE);
	const page = helios::newpage()!;
	const ptr = helios::map(rt::vspace, 0, map_flags::W, page)!: uintptr;
	return (page, ptr, n);
};
