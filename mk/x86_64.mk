HAREARCH = x86_64
QEMU = qemu-system-x86_64
QBEARCH = amd64_sysv
QEMUFLAGS = -cpu qemu64,fsgsbase

all: boot.iso

# XXX: This code probably has too much knowledge of Helios internals
BOOT=$(HELIOS)/boot/+x86_64/boot.bin

$(BOOT):
	cd ../helios && make boot/+x86_64/boot.bin

.PHONY: $(BOOT)

boot.iso: $(BOOT) syslinux.cfg $(HELIOS)/helios sbin/sysinit bootstrap.tar
	mkdir -p isodir
	cp syslinux.cfg isodir/syslinux.cfg
	cp $(SYSLINUX)/mboot.c32 isodir/mboot.c32
	cp $(SYSLINUX)/ldlinux.c32 isodir/ldlinux.c32
	cp $(SYSLINUX)/libcom32.c32 isodir/libcom32.c32
	cp $(SYSLINUX)/isolinux.bin isodir/isolinux.bin

	cp $(BOOT) isodir/boot.bin
	cp $(HELIOS)/helios isodir/helios
	cp sbin/sysinit isodir/sysinit
	cp bootstrap.tar isodir/bootstrap.tar

	mkisofs -o $@ -b isolinux.bin -c boot.cat \
		-no-emul-boot -boot-load-size 4 -boot-info-table \
		-iso-level 2 isodir
	isohybrid $@

disk.img:
	# Temporary test disk image
	qemu-img create -f raw disk.img 64M
	echo hello world | dd of=disk.img conv=notrunc

nographic: boot.iso disk.img
	$(QEMU) $(QEMUFLAGS) \
		-m 1G -no-reboot -no-shutdown \
		-drive file=boot.iso,format=raw \
		-drive id=disk,file=disk.img,if=none,format=raw \
		-device ahci,id=ahci \
		-device ide-hd,drive=disk,bus=ahci.0 \
		-display none \
		-serial stdio

run: boot.iso disk.img
	$(QEMU) $(QEMUFLAGS) \
		-m 1G -no-reboot -no-shutdown \
		-drive file=boot.iso,format=raw \
		-drive id=disk,file=disk.img,if=none,format=raw \
		-device ahci,id=ahci \
		-device ide-hd,drive=disk,bus=ahci.0 \
		-display sdl \
		-serial stdio

nographic-gdb: boot.iso disk.img
	$(QEMU) $(QEMUFLAGS) \
		-s -S -m 1G -no-reboot -no-shutdown \
		-drive file=boot.iso,format=raw \
		-drive id=disk,file=disk.img,if=none,format=raw \
		-device ahci,id=ahci \
		-device ide-hd,drive=disk,bus=ahci.0 \
		-display none \
		-serial stdio

install-arch: $(BOOT)
	mkdir -p "$(DESTDIR)$(LIBEXECDIR)"/helios
	install -Dm755 $(BOOT) "$(DESTDIR)$(LIBEXECDIR)"/helios/boot-$(ARCH)
