namespace sys;
use io;

# Device manager interface.
interface devmgr {
	# Returns a capability associated with the specified device by its
	# interface ID and serial number.
	call open{; out}(iface: u32, serial: u32) void;

	# Writes a list of devices available on this system to the provided
	# pages of memory, returning the total number of devices matching the
	# filter criteria. The device list is written as an array of the
	# following structures:
	#
	#	type device = struct {
	#		iface: u32,
	#		serial: u32,
	#	};
	#
	# Only up to "n" devices are returned.
	#
	# If any "id" arguments are provided, they are used to filter the
	# device list to only those which implement the specified interface
	# IDs. If no IDs are provided, all available devices are returned.
	call query{pages: page...}(
		buf: uintptr,
		n: size,
	) size;

	# Registers a notification to be signalled when a device is hotplugged,
	# so that the device list may be rescanned.
	#send hotplug{note: notification} void;
};

# Device driver loader.
interface devloader {
	# Loads an ELF image into a process, reading the driver manifest and
	# installing additional driver capabilities as necessary. Used instead
	# of [[proc::load]] for driver processes.
	call load{proc: process, image: io::file}() void;
};

# Device driver registry.
interface devregistry {
	# Creates a new device implementing the given interface ID using the
	# provided endpoint capability and returns its assigned serial number.
	call new{; out}(iface: u32) u32;
};
