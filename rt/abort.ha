fn abort_writestr(s: str) size = {
	let buf = *(&s: *[]u8);
	return writecons(buf);
};

export @symbol("rt.abort") @noreturn fn _abort(loc: str, msg: str) void = {
	abort_writestr("Abort: ");
	abort_writestr(loc);
	abort_writestr(": ");
	abort_writestr(msg);
	abort_writestr("\n");
	for (true) void;
};

// See harec:include/gen.h
const reasons: [_]str = [
	"slice or array access out of bounds",			// 0
	"type assertion failed",				// 1
	"out of memory",					// 2
	"static insert/append exceeds slice capacity",		// 3
	"execution reached unreachable code (compiler bug)",	// 4
	"slice allocation capacity smaller than initializer",	// 5
	"assertion failed",					// 6
	"error occured",					// 7
	"return from @noreturn function",			// 8
];

export @noreturn fn abort_fixed(loc: str, i: int) void = {
	_abort(loc, reasons[i]);
};
